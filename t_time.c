#include <stdio.h>
#include <stdlib.h>
#include "t_time.h"

struct t_time {
	int h, m, s;
};

struct t_timetable {

	char *valor;
	struct t_time;
};

T_time *criar_time() {
	return NULL;
}

T_time *inserir(T_time *h, int i) {
	T_time *novo = (T_time*)malloc(sizeof(T_time));
	novo->temp = i;
	novo->prox = h;

	return novo;
}

void liberar(T_time *l) {

	T_time *p = l;
	while(p != NULL) {
		T_time *t = p->prox;
		p = t;
	}
}

int time_cmp(T_time h1, T_time h2){

	if(h1 > h2)
		return 1;
	else if(h1 < h2)
		return -1;
	else
		return 0;

}

int min() 
{  return keys[0];  }
int  max() 
{  return keys[N-1];  }
