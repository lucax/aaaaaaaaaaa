typedef struct t_time T_time;

typedef struct t_timetable T_timetable;

T_time *criar_time();

T_time *inserir(T_time *h, int info);

void liberar(T_time *l);
